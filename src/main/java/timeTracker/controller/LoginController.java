package timeTracker.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import timeTracker.service.LoginService;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequiredArgsConstructor
@RequestMapping("login")
public class LoginController {
  private final LoginService loginService;

  @ApiOperation("���������� �������� ������")
  @GetMapping
  public void getLoginPage(HttpServletResponse response) {
    loginService.getLoginPage(response);
  }

}
