package timeTracker.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletResponse;

@Service
@RequiredArgsConstructor
@Slf4j
public class LoginService {
  public void getLoginPage(HttpServletResponse response) {
    try {
      InputStream loginPageStream = this.getClass().getClassLoader().getResourceAsStream("login.html");
      if (loginPageStream == null) {
        throw new IllegalArgumentException("Не удалось загрузить страницу входа");
      }
      StreamUtils.copy(loginPageStream, response.getOutputStream());
    } catch (IOException e) {
      log.error("Failed to get login page", e);
      throw new IllegalArgumentException("Страница входа недоступна");
    }
  }
}
